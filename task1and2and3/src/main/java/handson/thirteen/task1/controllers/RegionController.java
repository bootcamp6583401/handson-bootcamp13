package handson.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.thirteen.task1.models.Region;
import handson.thirteen.task1.models.World;
import handson.thirteen.task1.repositories.RegionRepository;
import handson.thirteen.task1.repositories.WorldRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/regions")
public class RegionController {
    @Autowired
    private RegionRepository regionRepo;

    @Autowired
    private WorldRepository worldRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewRegion(@RequestBody Region region) {

        Region n = new Region();
        n.setName(region.getName());
        regionRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Region> getAllRegions() {
        return regionRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Region> deleteRegion(@PathVariable int id) {
        Optional<Region> optionalRegion = regionRepo.findById(id);
        if (optionalRegion.isPresent()) {
            Region foundRegion = optionalRegion.get();
            regionRepo.delete(foundRegion);
            return new ResponseEntity<>(foundRegion, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Region> getRegionById(@PathVariable("id") int id) {
        Optional<Region> userData = regionRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Region> updateRegion(@PathVariable("id") int id, @RequestBody Region region) {
        Optional<Region> regionData = regionRepo.findById(id);

        if (regionData.isPresent()) {
            Region _region = regionData.get();
            _region.setName(region.getName());
            return new ResponseEntity<>(regionRepo.save(_region), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/world/{worldId}")
    public ResponseEntity<Region> setWorld(@PathVariable("id") int id, @PathVariable("worldId") int worldId) {
        Optional<Region> regionData = regionRepo.findById(id);
        Optional<World> world = worldRepo.findById(worldId);

        if (regionData.isPresent() && world.isPresent()) {
            Region _region = regionData.get();
            World _world = world.get();

            _region.setWorld(_world);

            return new ResponseEntity<>(regionRepo.save(_region), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}