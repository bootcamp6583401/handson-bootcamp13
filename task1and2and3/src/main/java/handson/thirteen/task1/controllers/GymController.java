package handson.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.thirteen.task1.models.Gym;
import handson.thirteen.task1.models.City;
import handson.thirteen.task1.repositories.GymRepository;
import handson.thirteen.task1.repositories.CityRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/gyms")
public class GymController {
    @Autowired
    private GymRepository gymRepo;

    @Autowired
    private CityRepository cityRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewGym(@RequestBody Gym gym) {

        Gym n = new Gym();
        n.setName(gym.getName());
        gymRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Gym> getAllGyms() {
        return gymRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Gym> deleteGym(@PathVariable int id) {
        Optional<Gym> optionalGym = gymRepo.findById(id);
        if (optionalGym.isPresent()) {
            Gym foundGym = optionalGym.get();
            gymRepo.delete(foundGym);
            return new ResponseEntity<>(foundGym, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Gym> getGymById(@PathVariable("id") int id) {
        Optional<Gym> userData = gymRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Gym> updateGym(@PathVariable("id") int id, @RequestBody Gym gym) {
        Optional<Gym> gymData = gymRepo.findById(id);

        if (gymData.isPresent()) {
            Gym _gym = gymData.get();
            _gym.setName(gym.getName());
            return new ResponseEntity<>(gymRepo.save(_gym), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/city/{cityId}")
    public ResponseEntity<Gym> setCity(@PathVariable("id") int id, @PathVariable("cityId") int cityId) {
        Optional<Gym> gymData = gymRepo.findById(id);
        Optional<City> city = cityRepo.findById(cityId);

        if (gymData.isPresent() && city.isPresent()) {
            Gym _gym = gymData.get();
            City _city = city.get();

            _gym.setCity(_city);

            return new ResponseEntity<>(gymRepo.save(_gym), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}