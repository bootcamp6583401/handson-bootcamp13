package handson.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.thirteen.task1.models.Move;
import handson.thirteen.task1.models.Pokemon;
import handson.thirteen.task1.models.Trainer;
import handson.thirteen.task1.repositories.MoveRepository;
import handson.thirteen.task1.repositories.PokemonRepository;
import handson.thirteen.task1.repositories.TrainerRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/pokemons")
public class PokemonController {
    @Autowired
    private PokemonRepository pokemonRepo;

    @Autowired
    private TrainerRepository trainerRepo;

    @Autowired
    private MoveRepository moveRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewPokemon(@RequestBody Pokemon poke) {

        Pokemon n = new Pokemon();
        n.setName(poke.getName());
        n.setAtk(poke.getAtk());
        n.setDef(poke.getDef());
        n.setSpd(poke.getSpd());
        pokemonRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Pokemon> getAllPokemons() {
        return pokemonRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Pokemon> deletePokemon(@PathVariable int id) {
        Optional<Pokemon> optionalPokemon = pokemonRepo.findById(id);
        if (optionalPokemon.isPresent()) {
            Pokemon foundPokemon = optionalPokemon.get();
            pokemonRepo.delete(foundPokemon);
            return new ResponseEntity<>(foundPokemon, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Pokemon> getPokemonById(@PathVariable("id") int id) {
        Optional<Pokemon> userData = pokemonRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Pokemon> updatePokemon(@PathVariable("id") int id, @RequestBody Pokemon poke) {
        Optional<Pokemon> pokeData = pokemonRepo.findById(id);

        if (pokeData.isPresent()) {
            Pokemon _poke = pokeData.get();
            _poke.setName(poke.getName());
            _poke.setAtk(poke.getAtk());
            _poke.setDef(poke.getDef());
            _poke.setSpd(poke.getSpd());
            return new ResponseEntity<>(pokemonRepo.save(_poke), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/trainer/{trainerId}")
    public ResponseEntity<Pokemon> setTrainer(@PathVariable("id") int id, @PathVariable("trainerId") int trainerId) {
        Optional<Pokemon> pokeData = pokemonRepo.findById(id);
        Optional<Trainer> trainer = trainerRepo.findById(trainerId);

        if (pokeData.isPresent() && trainer.isPresent()) {
            Pokemon _poke = pokeData.get();
            Trainer _trainer = trainer.get();

            _poke.setTrainer(_trainer);

            return new ResponseEntity<>(pokemonRepo.save(_poke), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/addMove/{moveId}")
    public ResponseEntity<Pokemon> addMove(@PathVariable("id") int id, @PathVariable("moveId") int moveId) {
        Optional<Pokemon> pokeData = pokemonRepo.findById(id);
        Optional<Move> move = moveRepo.findById(moveId);

        if (pokeData.isPresent() && move.isPresent()) {
            Pokemon _poke = pokeData.get();
            Move _move = move.get();

            _poke.getOwnedMoves().add(_move);
            _move.getPokemons().add(_poke);

            moveRepo.save(_move);

            return new ResponseEntity<>(pokemonRepo.save(_poke), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}