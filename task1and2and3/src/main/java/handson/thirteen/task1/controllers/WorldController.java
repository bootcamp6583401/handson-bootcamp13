package handson.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.thirteen.task1.models.World;
import handson.thirteen.task1.repositories.WorldRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/worlds")
public class WorldController {
    @Autowired
    private WorldRepository worldRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewWorld(@RequestBody World world) {

        World n = new World();
        n.setName(world.getName());
        worldRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<World> getAllWorlds() {
        return worldRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<World> deleteWorld(@PathVariable int id) {
        Optional<World> optionalWorld = worldRepo.findById(id);
        if (optionalWorld.isPresent()) {
            World foundWorld = optionalWorld.get();
            worldRepo.delete(foundWorld);
            return new ResponseEntity<>(foundWorld, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<World> getWorldById(@PathVariable("id") int id) {
        Optional<World> userData = worldRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<World> updateWorld(@PathVariable("id") int id, @RequestBody World world) {
        Optional<World> worldData = worldRepo.findById(id);

        if (worldData.isPresent()) {
            World _world = worldData.get();
            _world.setName(world.getName());
            return new ResponseEntity<>(worldRepo.save(_world), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}