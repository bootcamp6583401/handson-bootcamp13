package handson.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.thirteen.task1.models.Move;
import handson.thirteen.task1.repositories.MoveRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/moves")
public class MoveController {
    @Autowired
    private MoveRepository moveRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewMove(@RequestBody Move move) {

        Move n = new Move();
        n.setName(move.getName());
        n.setPower(move.getPower());
        moveRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Move> getAllMoves() {
        return moveRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Move> deleteMove(@PathVariable int id) {
        Optional<Move> optionalMove = moveRepo.findById(id);
        if (optionalMove.isPresent()) {
            Move foundMove = optionalMove.get();
            moveRepo.delete(foundMove);
            return new ResponseEntity<>(foundMove, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Move> getMoveById(@PathVariable("id") int id) {
        Optional<Move> userData = moveRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Move> updateMove(@PathVariable("id") int id, @RequestBody Move move) {
        Optional<Move> moveData = moveRepo.findById(id);

        if (moveData.isPresent()) {
            Move _move = moveData.get();
            _move.setName(move.getName());
            _move.setPower(move.getPower());
            return new ResponseEntity<>(moveRepo.save(_move), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}