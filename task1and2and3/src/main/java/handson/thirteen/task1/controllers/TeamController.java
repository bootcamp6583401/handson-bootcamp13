package handson.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.thirteen.task1.models.Team;
import handson.thirteen.task1.models.Trainer;
import handson.thirteen.task1.repositories.TeamRepository;
import handson.thirteen.task1.repositories.TrainerRepository;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/teams")
public class TeamController {
    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private TrainerRepository trainerRepository;

    @PostMapping(path = "/")
    public @ResponseBody String addNewTeam(@RequestParam String name) {

        Team n = new Team();
        n.setName(name);
        teamRepository.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Team> getAllTeams() {
        return teamRepository.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Team> deleteTeam(@PathVariable int id) {
        Optional<Team> optionalTeam = teamRepository.findById(id);
        if (optionalTeam.isPresent()) {
            Team foundTeam = optionalTeam.get();
            teamRepository.delete(foundTeam);
            return new ResponseEntity<>(foundTeam, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Team> getTeamById(@PathVariable("id") int id) {
        Optional<Team> teamData = teamRepository.findById(id);
        List<Trainer> trainers = trainerRepository.findByTeamId(id);

        if (teamData.isPresent()) {
            Team team = teamData.get();
            team.setTrainerList(trainers);
            return new ResponseEntity<>(team, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/trainers")
    public ResponseEntity<List<Trainer>> getTeamBlogs(@PathVariable("id") int id) {
        Optional<Team> teamData = teamRepository.findById(id);
        List<Trainer> trainers = trainerRepository.findByTeamId(id);

        if (teamData.isPresent()) {
            return new ResponseEntity<>(trainers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Team> updateTeam(@PathVariable("id") int id, @RequestBody Team team) {
        Optional<Team> tutorialData = teamRepository.findById(id);

        if (tutorialData.isPresent()) {
            Team _team = tutorialData.get();
            _team.setName(team.getName());
            return new ResponseEntity<>(teamRepository.save(_team), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}