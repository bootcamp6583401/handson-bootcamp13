package handson.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.thirteen.task1.models.Pokemon;
import handson.thirteen.task1.models.Team;
import handson.thirteen.task1.models.Trainer;
import handson.thirteen.task1.repositories.PokemonRepository;
import handson.thirteen.task1.repositories.TeamRepository;
import handson.thirteen.task1.repositories.TrainerRepository;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/trainers")
public class TrainerController {
    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private PokemonRepository pokemonRepository;

    @Autowired
    private TeamRepository teamRepository;

    @PostMapping(path = "/")
    public @ResponseBody String addNewTrainer(@RequestParam String name) {

        Trainer n = new Trainer();
        n.setName(name);
        trainerRepository.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Trainer> getAllTrainers() {
        return trainerRepository.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Trainer> deleteTrainer(@PathVariable int id) {
        Optional<Trainer> optionalTrainer = trainerRepository.findById(id);
        if (optionalTrainer.isPresent()) {
            Trainer foundTrainer = optionalTrainer.get();
            trainerRepository.delete(foundTrainer);
            return new ResponseEntity<>(foundTrainer, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Trainer> getTrainerById(@PathVariable("id") int id) {
        Optional<Trainer> trainerData = trainerRepository.findById(id);
        List<Pokemon> pokemons = pokemonRepository.findByTrainerId(id);

        if (trainerData.isPresent()) {
            Trainer trainer = trainerData.get();
            trainer.setPokemonList(pokemons);
            return new ResponseEntity<>(trainer, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/pokemons")
    public ResponseEntity<List<Pokemon>> getTrainerBlogs(@PathVariable("id") int id) {
        Optional<Trainer> trainerData = trainerRepository.findById(id);
        List<Pokemon> pokemons = pokemonRepository.findByTrainerId(id);

        if (trainerData.isPresent()) {
            return new ResponseEntity<>(pokemons, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Trainer> updateTrainer(@PathVariable("id") int id, @RequestBody Trainer trainer) {
        Optional<Trainer> tutorialData = trainerRepository.findById(id);

        if (tutorialData.isPresent()) {
            Trainer _trainer = tutorialData.get();
            _trainer.setName(trainer.getName());
            return new ResponseEntity<>(trainerRepository.save(_trainer), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/team/{teamId}")
    public ResponseEntity<Trainer> setTrainer(@PathVariable("id") int id, @PathVariable("teamId") int teamId) {
        Optional<Trainer> trainerData = trainerRepository.findById(id);
        Optional<Team> teamData = teamRepository.findById(teamId);

        if (trainerData.isPresent() && teamData.isPresent()) {
            Trainer _trainer = trainerData.get();
            Team _teamData = teamData.get();

            _trainer.setTeam(_teamData);

            return new ResponseEntity<>(trainerRepository.save(_trainer), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}