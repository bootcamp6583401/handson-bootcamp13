package handson.thirteen.task1.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.thirteen.task1.models.World;

public interface WorldRepository extends CrudRepository<World, Integer> {

}