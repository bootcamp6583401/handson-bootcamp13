package handson.thirteen.task1.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.thirteen.task1.models.Team;

public interface TeamRepository extends CrudRepository<Team, Integer> {

}