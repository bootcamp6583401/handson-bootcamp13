package handson.thirteen.task1.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.thirteen.task1.models.Move;

public interface MoveRepository extends CrudRepository<Move, Integer> {

}