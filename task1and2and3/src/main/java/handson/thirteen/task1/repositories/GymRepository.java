package handson.thirteen.task1.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import handson.thirteen.task1.models.Gym;

public interface GymRepository extends CrudRepository<Gym, Integer> {
    List<Gym> findByCityId(int city_id);

}