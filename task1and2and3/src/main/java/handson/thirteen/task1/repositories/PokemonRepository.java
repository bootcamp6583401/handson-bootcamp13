package handson.thirteen.task1.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import handson.thirteen.task1.models.Pokemon;

public interface PokemonRepository extends CrudRepository<Pokemon, Integer> {
    List<Pokemon> findByTrainerId(int trainer_id);

}