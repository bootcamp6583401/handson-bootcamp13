package handson.thirteen.task1.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import handson.thirteen.task1.models.Trainer;

public interface TrainerRepository extends CrudRepository<Trainer, Integer> {
    List<Trainer> findByTeamId(int team_id);

}