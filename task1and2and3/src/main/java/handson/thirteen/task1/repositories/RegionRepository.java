package handson.thirteen.task1.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import handson.thirteen.task1.models.Region;

public interface RegionRepository extends CrudRepository<Region, Integer> {
    List<Region> findByWorldId(int world_id);

}